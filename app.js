//Just checking Git Version 3 from Github
//Final check then good to go on Windows
//commit check from Linux

var express = require("express")
var path = require("path");

var mongoose =require("mongoose");
var cookieParser = require("cookie-parser");
var passport = require("passport");
var session = require("express-session");
var flash = require("connect-flash");
var params = require("./params/params");

var passportjs = require("./passportjs.js");

var bodyParser = require("body-parser");

//var routes = require("./routes");

var app = express();



mongoose.connect(params.DATABASECON,{useUnifiedTopology: true, useNewUrlParser:true, useCreateIndex:true});
passportjs();


app.set("port",process.env.port || 3000);

app.set("views", path.join(__dirname, "pages"));

app.set("view engine","ejs");

app.use(bodyParser.urlencoded({extended:false}));

app.use(cookieParser());

app.use(session({
    secret:"hcgchgchvhnbbmb",
    resave:false,
    saveUninitialized:false

}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(express.static('public'))



app.use("/", require("./routes/web/"));

app.use("/api", require("./routes/api/"));

app.listen(app.get("port"),function() {

    console.log("Server on port " + app.get("port"));

})





